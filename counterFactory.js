function counterFactory(){
    count=0;
    const result={increment:function(){
        return count+=1;
    },
    decrement:function(){
        return count-=1;
    }
  }
  //result.increment();
  //result.decrement();
  return result;
}
module.exports=counterFactory;