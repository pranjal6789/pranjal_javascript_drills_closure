function cacheFunction(cb){
    let cache={};
    return function foo(...args){
        if(!args || args.length==0){
            return {};
        }
        for(var i=0;i<args.length;i++){
            if(args[i] in cache){
                return cache;
            }
            else{
                cache[args[i]]=cb(args);
            }

            return cache;
    }   
}
}
module.exports=cacheFunction;