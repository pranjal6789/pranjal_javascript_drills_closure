function limitFunctionCallCount(cb,n){
       return ()=>{
        if(n>0){
            n=n-1;
            cb();
        }     
    } 
}

module.exports=limitFunctionCallCount;